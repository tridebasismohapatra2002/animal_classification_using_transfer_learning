import streamlit as st
from PIL import Image
from classify import classify_image

st.markdown("<h1 style='text-align: center;'>Animal Classification Project</h1>", unsafe_allow_html=True)

st.markdown("<h6 style='text-align: center;'>Please provide the image of an animal and this system will classify the image for you</h6>", unsafe_allow_html=True)

st.markdown("<h4 style='text-align: center;'>Choose An Image</h4>", unsafe_allow_html=True)

uploaded_file = st.file_uploader("")

if uploaded_file is not None:
    image = Image.open(uploaded_file).convert("RGB")
    with st.spinner('Classifying...'):
        Animal_name = classify_image(image).title()
    
    st.image(image, caption='Uploaded Image.', use_column_width=True)
    st.markdown(f"<h2 style='text-align: center;'>{Animal_name}</h2>", unsafe_allow_html=True)

        
